# CMakeIt
A collection of CMake modules to build programs from 'Visual Studio' file structure-like projects, and well-structure project layouts (public and private include folders, source folders), using CMake build system. Also features pre compiled headers support, unit tests, installation ('make install' style), packaging, etc.
