#
# CMakeIt - a collection of CMake modules to build programs from 'Visual Studio'-like 
# projects, and well-structure project layouts (public and private include folders,
# source folders), using CMake build system. Also features pre compiled headers
# support, unit tests, installation ('make install' style), packaging, etc.
#
# Copyright (C) 2013, Virgilio Alexandre Fornazin
#
# This library is free software; you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software 
# Foundation; either version 3 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
# PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along 
# with this library; if not, please write to the Free Software Foundation, Inc., 
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

#
# CMakeIt-TreeScanner.cmake - CMakeIt script to scan a directory tree for CMakeLists.txt projects
#

# Check if CMakeIt.cmake was included before
IF (NOT CMAKEIT_INCLUDED)
    MESSAGE(FATAL_ERROR "CMakeIt.cmake was not included before trying to automatic scan directory tree")
ENDIF ()

MESSAGE(STATUS "Scanning projects...")

LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST ".vs")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST ".vscode")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST ".tf")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST ".svn")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST ".git")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST ".gitmodule")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST ".gitmodules")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST ".gitsubmodule")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST ".gitsubmodules")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST ".build")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST ".output")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST "CVS")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST "3rd")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST "bin")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST "obj")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST "build")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST "submodule")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST "submodules")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST "CMakeIt")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST "Debug")
LIST(APPEND INTERNAL_GLOB_EXCLUSION_LIST "Release")

FILE(GLOB_RECURSE INTERNAL_GLOB_TREESCANNER_PROJECTS FOLLOW_SYMLINKS
	LIST_DIRECTORIES TRUE RELATIVE "${CMAKE_SOURCE_DIR}" "${CMAKE_SOURCE_DIR}/CMakeLists.txt")

LIST(REMOVE_ITEM INTERNAL_GLOB_TREESCANNER_PROJECTS "CMakeLists.txt")

FOREACH (INTERNAL_GLOB_TREESCANNER_PROJECT ${INTERNAL_GLOB_TREESCANNER_PROJECTS})

	STRING(REPLACE "CMakeLists.txt" ${INTERNAL_GLOB_TREESCANNER_PROJECT} INTERNAL_GLOB_TREESCANNER_PROJECT_DIRECTORY)
	MESSAGE(STATUS "Found CMake project in ${INTERNAL_GLOB_TREESCANNER_PROJECT_DIRECTORY} ...")
	
	ADD_SUBDIRECTORY(${INTERNAL_GLOB_TREESCANNER_PROJECT_DIRECTORY})

	MATH(EXPR INTERNAL_GLOB_TREESCANNER_PROJECT_COUNT "${INTERNAL_GLOB_TREESCANNER_PROJECT_COUNT} + 1")
	
ENDFOREACH ()

IF (NOT INTERNAL_GLOB_TREESCANNER_PROJECT_COUNT)
	MESSAGE(STATUS "* WARNING: CMakeIt-TreeScanner found no project(s) for build")
ELSE ()
	
	MESSAGE(STATUS "CMakeIt-TreeScanner found ${INTERNAL_GLOB_TREESCANNER_PROJECT_COUN} project(s) for build")

	UNSET(INTERNAL_GLOB_TREESCANNER_PROJECT_COUNT)
	
ENDIF ()

UNSET(INTERNAL_GLOB_TREESCANNER_PROJECTS)
