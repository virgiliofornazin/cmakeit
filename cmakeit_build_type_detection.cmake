#
# CMakeIt - a collection of CMake modules to build programs from 'Visual Studio'-like 
# projects, and well-structure project layouts (public and private include folders,
# source folders), using CMake build system. Also features pre compiled headers
# support, unit tests, installation ('make install' style), packaging, etc.
#
# Copyright (C) 2013, Virgilio Alexandre Fornazin
#
# This library is free software; you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software 
# Foundation; either version 3 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
# PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along 
# with this library; if not, please write to the Free Software Foundation, Inc., 
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

#
# cmakeit_build_type_detection.cmake - detect build type (debug, release, etc)
#

IF (NOT CMAKEIT_HIDE_BANNER)
    MESSAGE(STATUS "Detecting build type...")
ENDIF ()

IF (NOT CMAKEIT_BUILD_TYPE AND (CMAKE_CONFIGURATION_TYPES STREQUAL "Debug"))
	SET(CMAKEIT_BUILD_TYPE ${CMAKEIT_BUILD_TYPE_DEBUG})
ENDIF ()

IF (NOT CMAKEIT_BUILD_TYPE AND (CMAKE_BUILD_TYPE STREQUAL "Debug"))
	SET(CMAKEIT_BUILD_TYPE ${CMAKEIT_BUILD_TYPE_DEBUG})
ENDIF ()

IF (NOT CMAKEIT_BUILD_TYPE AND (CMAKE_CONFIGURATION_TYPES STREQUAL "RelWithDebInfo"))
	SET(CMAKEIT_BUILD_TYPE ${CMAKEIT_BUILD_TYPE_RELWITHDEBINFO})
ENDIF ()

IF (NOT CMAKEIT_BUILD_TYPE AND (CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo"))
	SET(CMAKEIT_BUILD_TYPE ${CMAKEIT_BUILD_TYPE_RELWITHDEBINFO})
ENDIF ()

IF (NOT CMAKEIT_BUILD_TYPE AND (CMAKE_CONFIGURATION_TYPES STREQUAL "Release"))
	SET(CMAKEIT_BUILD_TYPE ${CMAKEIT_BUILD_TYPE_RELEASE})
ENDIF ()

IF (NOT CMAKEIT_BUILD_TYPE AND (CMAKE_BUILD_TYPE STREQUAL "Release"))
	SET(CMAKEIT_BUILD_TYPE ${CMAKEIT_BUILD_TYPE_RELEASE})
ENDIF ()

IF (NOT ((CMAKEIT_BUILD_TYPE STREQUAL ${CMAKEIT_BUILD_TYPE_DEBUG}) OR (CMAKEIT_BUILD_TYPE STREQUAL ${CMAKEIT_BUILD_TYPE_RELWITHDEBINFO}) OR (CMAKEIT_BUILD_TYPE STREQUAL ${CMAKEIT_BUILD_TYPE_RELEASE})))
	SET(CMAKEIT_BUILD_TYPE ${CMAKEIT_BUILD_TYPE_RELEASE})
ENDIF ()

SET(CMAKE_CONFIGURATION_TYPES ${CMAKEIT_BUILD_TYPE} CACHE STRING "" FORCE)
SET(CMAKE_BUILD_TYPE ${CMAKEIT_BUILD_TYPE} CACHE STRING "" FORCE)
