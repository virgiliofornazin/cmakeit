#
# CMakeIt - a collection of CMake modules to build programs from 'Visual Studio'-like 
# projects, and well-structure project layouts (public and private include folders,
# source folders), using CMake build system. Also features pre compiled headers
# support, unit tests, installation ('make install' style), packaging, etc.
#
# Copyright (C) 2013, Virgilio Alexandre Fornazin
#
# This library is free software; you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software 
# Foundation; either version 3 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
# PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along 
# with this library; if not, please write to the Free Software Foundation, Inc., 
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

#
# cmakeit_project_tree_scanner.cmake - scan for source files in project tree
#

FILE(GLOB_RECURSE INTERNAL_GLOB_PROJECT_TREESCANNER_PRIVATE_INCLUDE FOLLOW_SYMLINKS
    LIST_DIRECTORIES TRUE RELATIVE "${CMAKE_SOURCE_DIR}" "${CMAKE_SOURCE_DIR}/include/private/*")

FILE(GLOB_RECURSE INTERNAL_GLOB_PROJECT_TREESCANNER_PUBLIC_INCLUDE FOLLOW_SYMLINKS
    LIST_DIRECTORIES TRUE RELATIVE "${CMAKE_SOURCE_DIR}" "${CMAKE_SOURCE_DIR}/include/public/*")

FILE(GLOB_RECURSE INTERNAL_GLOB_PROJECT_TREESCANNER_INCLUDE FOLLOW_SYMLINKS
    LIST_DIRECTORIES TRUE RELATIVE "${CMAKE_SOURCE_DIR}" "${CMAKE_SOURCE_DIR}/include/*")

FILE(GLOB_RECURSE INTERNAL_GLOB_PROJECT_TREESCANNER_SRC FOLLOW_SYMLINKS
    LIST_DIRECTORIES TRUE RELATIVE "${CMAKE_SOURCE_DIR}" "${CMAKE_SOURCE_DIR}/src/*")

FILE(GLOB_RECURSE INTERNAL_GLOB_PROJECT_TREESCANNER_ALL FOLLOW_SYMLINKS
    LIST_DIRECTORIES TRUE RELATIVE "${CMAKE_SOURCE_DIR}" "${CMAKE_SOURCE_DIR}/*")

SET(INTERNAL_GLOB_PROJECT_TREESCANNER_PRIVATE_INCLUDE_LENGTH 0)
SET(INTERNAL_GLOB_PROJECT_TREESCANNER_PUBLIC_INCLUDE_LENGTH 0)
SET(INTERNAL_GLOB_PROJECT_TREESCANNER_INCLUDE_LENGTH 0)
SET(INTERNAL_GLOB_PROJECT_TREESCANNER_SRC_LENGTH 0)
SET(INTERNAL_GLOB_PROJECT_TREESCANNER_ALL_LENGTH 0)

IF (INTERNAL_GLOB_PROJECT_TREESCANNER_PRIVATE_INCLUDE)
	LIST(LENGTH INTERNAL_GLOB_PROJECT_TREESCANNER_PRIVATE_INCLUDE INTERNAL_GLOB_PROJECT_TREESCANNER_PRIVATE_INCLUDE_LENGTH)
ENDIF ()

IF (INTERNAL_GLOB_PROJECT_TREESCANNER_PUBLIC_INCLUDE)
	LIST(LENGTH INTERNAL_GLOB_PROJECT_TREESCANNER_PUBLIC_INCLUDE INTERNAL_GLOB_PROJECT_TREESCANNER_PUBLIC_INCLUDE_LENGTH)
ENDIF ()

IF (INTERNAL_GLOB_PROJECT_TREESCANNER_INCLUDE)
	LIST(LENGTH INTERNAL_GLOB_PROJECT_TREESCANNER_INCLUDE INTERNAL_GLOB_PROJECT_TREESCANNER_INCLUDE_LENGTH)
ENDIF ()

IF (INTERNAL_GLOB_PROJECT_TREESCANNER_SRC)
	LIST(LENGTH INTERNAL_GLOB_PROJECT_TREESCANNER_SRC INTERNAL_GLOB_PROJECT_TREESCANNER_SRC_LENGTH)
ENDIF ()

IF (INTERNAL_GLOB_PROJECT_TREESCANNER_ALL)
	LIST(LENGTH INTERNAL_GLOB_PROJECT_TREESCANNER_ALL INTERNAL_GLOB_PROJECT_TREESCANNER_ALL_LENGTH)
ENDIF ()

IF (NOT CMAKEIT_SOURCES_LAYOUT)

    IF ((INTERNAL_GLOB_PROJECT_TREESCANNER_PRIVATE_INCLUDE_LENGTH GREATER 0) OR (INTERNAL_GLOB_PROJECT_TREESCANNER_PUBLIC_INCLUDE_LENGTH GREATER 0))

        IF (INTERNAL_GLOB_PROJECT_TREESCANNER_SRC_LENGTH GREATER 0)
            SET(CMAKEIT_SOURCES_LAYOUT ${CMAKEIT_SOURCES_LAYOUT_PRIVATE_PUBLIC_INCLUDE_AND_SOURCE_DIRECTORIES})
        ENDIF ()
    
    ENDIF ()

ENDIF ()

IF (NOT CMAKEIT_SOURCES_LAYOUT)

    IF (INTERNAL_GLOB_PROJECT_TREESCANNER_INCLUDE_LENGTH GREATER 0)

        IF (INTERNAL_GLOB_PROJECT_TREESCANNER_SRC_LENGTH GREATER 0)
            SET(CMAKEIT_SOURCES_LAYOUT ${CMAKEIT_SOURCES_LAYOUT_PUBLIC_INCLUDE_AND_SOURCE_DIRECTORIES})
        ELSE ()
            SET(CMAKEIT_SOURCES_LAYOUT ${CMAKEIT_SOURCES_LAYOUT_PUBLIC_INCLUDE_DIRECTORY})
        ENDIF ()

    ENDIF ()

ENDIF ()

IF (NOT CMAKEIT_SOURCES_LAYOUT)

    IF (INTERNAL_GLOB_PROJECT_TREESCANNER_SRC_LENGTH GREATER 0)
        SET(CMAKEIT_SOURCES_LAYOUT ${CMAKEIT_SOURCES_LAYOUT_SOURCE_DIRECTORY})
    ENDIF ()

ENDIF ()

IF (NOT CMAKEIT_SOURCES_LAYOUT)
    SET(CMAKEIT_SOURCES_LAYOUT ${CMAKEIT_SOURCES_LAYOUT_ENTIRE_DIRECTORY})
ENDIF ()

IF (NOT CMAKEIT_HIDE_BANNER)
	MESSAGE(STATUS "Directory structure: ${CMAKEIT_SOURCES_LAYOUT}")
ENDIF ()

UNSET(INTERNAL_PRIVATE_INCLUDE_FILES_TO_CONFIGURE)
UNSET(INTERNAL_PUBLIC_INCLUDE_FILES_TO_CONFIGURE)
UNSET(INTERNAL_SOURCE_FILES_TO_CONFIGURE)

UNSET(CMAKEIT_PROJECT_PRIVATE_INCLUDE_FILES)
UNSET(CMAKEIT_PROJECT_PUBLIC_INCLUDE_FILES)
UNSET(CMAKEIT_PROJECT_SOURCE_FILES)

IF (CMAKEIT_SOURCES_LAYOUT STREQUAL ${CMAKEIT_SOURCES_LAYOUT_PRIVATE_PUBLIC_INCLUDE_AND_SOURCE_DIRECTORIES})

    FOREACH (INTERNAL_PRIVATE_INCLUDE_FILE ${INTERNAL_GLOB_PROJECT_TREESCANNER_PRIVATE_INCLUDE})

        STRING(FIND ${INTERNAL_PRIVATE_INCLUDE_FILE} ".in_cmake." INTERNAL_PRIVATE_INCLUDE_FILE_IN_CMAKE_INDEX)

        IF (NOT (INTERNAL_PRIVATE_INCLUDE_FILE_IN_CMAKE_INDEX EQUAL -1))

            LIST(APPEND INTERNAL_PRIVATE_INCLUDE_FILES_TO_CONFIGURE ${INTERNAL_PRIVATE_INCLUDE_FILE})
            
            STRING(REPLACE ".in_cmake." "." INTERNAL_PRIVATE_INCLUDE_FILE_CONFIGURED ${INTERNAL_PRIVATE_INCLUDE_FILE})

        ENDIF ()

    ENDFOREACH ()

ENDIF ()

UNSET(INTERNAL_GLOB_PROJECT_TREESCANNER_PRIVATE_INCLUDE_LENGTH)
UNSET(INTERNAL_GLOB_PROJECT_TREESCANNER_PUBLIC_INCLUDE_LENGTH)
UNSET(INTERNAL_GLOB_PROJECT_TREESCANNER_INCLUDE_LENGTH)
UNSET(INTERNAL_GLOB_PROJECT_TREESCANNER_SRC_LENGTH)
UNSET(INTERNAL_GLOB_PROJECT_TREESCANNER_ALL_LENGTH)

UNSET(INTERNAL_GLOB_PROJECT_TREESCANNER_PRIVATE_INCLUDE)
UNSET(INTERNAL_GLOB_PROJECT_TREESCANNER_PUBLIC_INCLUDE)
UNSET(INTERNAL_GLOB_PROJECT_TREESCANNER_INCLUDE)
UNSET(INTERNAL_GLOB_PROJECT_TREESCANNER_SRC)
UNSET(INTERNAL_GLOB_PROJECT_TREESCANNER_ALL)
