#
# CMakeIt - a collection of CMake modules to build programs from 'Visual Studio'-like 
# projects, and well-structure project layouts (public and private include folders,
# source folders), using CMake build system. Also features pre compiled headers
# support, unit tests, installation ('make install' style), packaging, etc.
#
# Copyright (C) 2013, Virgilio Alexandre Fornazin
#
# This library is free software; you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software 
# Foundation; either version 3 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
# PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along 
# with this library; if not, please write to the Free Software Foundation, Inc., 
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

#
# cmakeit_project_template.cmake - CMakeIt base project template declaration
#

# Check if CMakeIt.cmake was included before
IF (NOT CMAKEIT_INCLUDED)
    MESSAGE(FATAL_ERROR "CMakeIt.cmake was not included before declaring the project")
ENDIF ()

#
# This module list the variables required for a project to be delared in CMakeIt
# build system. These are below:
#
# CMAKEIT_PROJECT_NAME - the project name (e.g.: cpp-foundation, boost)
#
IF (NOT CMAKEIT_PROJECT_NAME)
    MESSAGE(FATAL_ERROR "Unknown CMakeIt project name (CMAKEIT_PROJECT_NAME not set)")
ENDIF ()

#
# CMAKEIT_PROJECT_SUBMODULE_NAME (optional) - the project submodule (e.g.: project boost, submodule asio)
#
IF (CMAKEIT_PROJECT_SUBMODULE_NAME)

    SET(INTERNAL_PROJECT_SUBMODULE_NAME_LENGTH 0)
    
    STRING(LENGTH ${CMAKEIT_PROJECT_SUBMODULE_NAME} INTERNAL_PROJECT_SUBMODULE_NAME_LENGTH)
    
    IF (INTERNAL_PROJECT_SUBMODULE_NAME_LENGTH EQUAL 0)
        MESSAGE(FATAL_ERROR "Unknown CMakeIt submodule name (CMAKEIT_PROJECT_SUBMODULE_NAME not set)")
    ENDIF ()

    UNSET(INTERNAL_PROJECT_SUBMODULE_NAME_LENGTH)
    
ENDIF ()

#
# CMAKEIT_MODULE_TYPE - the project module type (see CMAKEIT_MODULE_TYPE_*
# in cmakeit_constants.cmake for valid values)
#
IF (NOT CMAKEIT_MODULE_TYPE)
    MESSAGE(FATAL_ERROR "Unknown CMakeIt project type (CMAKEIT_MODULE_TYPE not set)")
ENDIF ()

#
# CMAKEIT_MODULE_SUBTYPE - the project module subtype (see CMAKEIT_MODULE_SUBTYPE_*
# in cmakeit_constants.cmake for valid values)
#
IF (NOT CMAKEIT_MODULE_SUBTYPE)
    MESSAGE(FATAL_ERROR "Unknown CMakeIt project subtype (CMAKEIT_MODULE_SUBTYPE not set)")
ENDIF ()

#
# CMAKEIT_MODULE_DESCRIPTION (optional) - the description of the module to be included in resource information
#
IF (NOT CMAKEIT_MODULE_DESCRIPTION)
    SET(CMAKEIT_MODULE_DESCRIPTION "${CMAKEIT_PROJECT_NAME} ${CMAKEIT_MODULE_NAME} ${CMAKEIT_MODULE_TYPE}")
ENDIF ()

#
# CMAKEIT_MODULE_VERSION_MAJOR (optional) - the major version number of the module to be included in resource information
#
IF (NOT CMAKEIT_MODULE_VERSION_MAJOR)
    SET(CMAKEIT_MODULE_VERSION_MAJOR 0)
ENDIF ()

#
# CMAKEIT_MODULE_VERSION_MINOR (optional) - the minor version number of the module to be included in resource information
#
IF (NOT CMAKEIT_MODULE_VERSION_MINOR)
    SET(CMAKEIT_MODULE_VERSION_MINOR 0)
ENDIF ()

#
# CMAKEIT_MODULE_REVISION_NUMBER (optional) - the revision version number of the module to be included in resource information
#
IF (NOT CMAKEIT_MODULE_REVISION_NUMBER)
    SET(CMAKEIT_MODULE_REVISION_NUMBER 0)
ENDIF ()

#
# CMAKEIT_MODULE_BUILD_NUMBER (optional) - the build number of the module to be included in resource information
#
IF (NOT CMAKEIT_MODULE_BUILD_NUMBER)
    SET(CMAKEIT_MODULE_BUILD_NUMBER 1)
ENDIF ()

#
# CMAKEIT_PRODUCT_OWNER (optional) - the name of the owner (company or person) of this project
#
IF (NOT CMAKEIT_PRODUCT_OWNER)
	SET(CMAKEIT_PRODUCT_OWNER "")
ENDIF ()

#
# CMAKEIT_PRODUCT_NAME (optional) - the name of the product this project belomgs to
#
IF (NOT CMAKEIT_PRODUCT_NAME)
	SET(CMAKEIT_PRODUCT_NAME "")
ENDIF ()

#
# CMAKEIT_DEPENDENCIES (optional) - the dependencies for the module for building
#
IF (NOT CMAKEIT_DEPENDENCIES)
    SET(CMAKEIT_DEPENDENCIES "")
ENDIF ()

#
# CMAKEIT_EXTERNAL_DEPENDENCIES (optional) - the external dependencies for the module for building
#
IF (NOT CMAKEIT_EXTERNAL_DEPENDENCIES)
    SET(CMAKEIT_EXTERNAL_DEPENDENCIES "")
ENDIF ()

#
# CMAKEIT_EXTERNAL_PUBLIC_INCLUDE_DIRECTORIES (optional) - the external public include directories
# dependencies for the module for building
#
IF (NOT CMAKEIT_EXTERNAL_PUBLIC_INCLUDE_DIRECTORIES)
    SET(CMAKEIT_EXTERNAL_PUBLIC_INCLUDE_DIRECTORIES "")
ENDIF ()

#
# CMAKEIT_EXTERNAL_PUBLIC_INCLUDE_DIRECTORIES (optional) - the external private include directories
# dependencies for the module for building
#
IF (NOT CMAKEIT_EXTERNAL_PRIVATE_INCLUDE_DIRECTORIES)
    SET(CMAKEIT_EXTERNAL_PRIVATE_INCLUDE_DIRECTORIES "")
ENDIF ()

# 
# CMAKEIT_PROJECT - the full project name for CMake
#
IF (NOT CMAKEIT_PROJECT_SUBMODULE_NAME)
	SET(CMAKEIT_PROJECT "${CMAKEIT_PROJECT_NAME}")
ELSE ()
	SET(CMAKEIT_PROJECT "${CMAKEIT_PROJECT_NAME}.${CMAKEIT_PROJECT_SUBMODULE_NAME}")
ENDIF ()

#
# CMAKEIT_MODULE_VERSION - the full version of the module to be included in resource information
#
SET(CMAKEIT_MODULE_VERSION "${CMAKEIT_MODULE_VERSION_MAJOR}.${CMAKEIT_MODULE_VERSION_MINOR}.${CMAKEIT_MODULE_REVISION_NUMBER}.${CMAKEIT_MODULE_BUILD_NUMBER}")

# All variables needed are validate, so we began to declare the project here
PROJECT(${CMAKEIT_PROJECT} VERSION ${CMAKEIT_MODULE_VERSION} DESCRIPTION ${CMAKEIT_MODULE_DESCRIPTION} LANGUAGES C CXX)

# Set CMAKEIT_PROJECT_INCLUDED variable
SET(CMAKEIT_PROJECT_INCLUDED ON)

# Scan for source files of the project
INCLUDE(cmakeit_project_tree_scanner)
