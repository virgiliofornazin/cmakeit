#
# CMakeIt - a collection of CMake modules to build programs from 'Visual Studio'-like 
# projects, and well-structure project layouts (public and private include folders,
# source folders), using CMake build system. Also features pre compiled headers
# support, unit tests, installation ('make install' style), packaging, etc.
#
# Copyright (C) 2013, Virgilio Alexandre Fornazin
#
# This library is free software; you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software 
# Foundation; either version 3 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
# PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along 
# with this library; if not, please write to the Free Software Foundation, Inc., 
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

#
# CMakeIt.cmake - main module of CMakeIt build system
#

# CMAKEIT_ROOT point to the root of CMakeIt module files. If not set, then 
# defaults to ${CMAKE_SOURCE_DIR}/CMakeIt
#
# Example: SET(CMAKEIT_ROOT ${CMAKE_SOURCE_DIR}/CMakeIt)
IF (NOT CMAKEIT_ROOT)
	SET(CMAKEIT_ROOT ${CMAKE_CURRENT_LIST_DIR})
ENDIF ()

# Add CMakeIt module path to CMake CMAKE_MODULE_PATH environment variable
IF (CMAKEIT_ROOT)
	LIST(APPEND CMAKE_MODULE_PATH ${CMAKEIT_ROOT})
ENDIF ()

# Configure variables to bootstrap CMakeIt
INCLUDE(cmakeit_version)
INCLUDE(cmakeit_variables)
INCLUDE(cmakeit_constants)

# Shows banner of CMake when called (and not configured to hide)
INCLUDE(cmakeit_banner)

# Perform detection of toolset, compiler and target that will be used for build
INCLUDE(cmakeit_detection)

# Define CMAKEIT_INCLUDED for next phase of build scripts
SET(CMAKEIT_INCLUDED ON)
